import {BrowserModule} from '@angular/platform-browser';
import {NgModule} from '@angular/core';
import { FormsModule } from '@angular/forms';


import {AppComponent} from './app.component';
import {MainComponent} from './main/main.component';
import {ConnectionService} from "./services/connection.service";
import {HttpClientModule} from "@angular/common/http";


@NgModule({
    declarations: [
        AppComponent,
        MainComponent
    ],
    imports: [
        BrowserModule,FormsModule,HttpClientModule,

    ],
    providers: [ConnectionService],
    bootstrap: [AppComponent]
})
export class AppModule {
}

import {Injectable} from '@angular/core';
import * as io from 'socket.io-client';
import {HttpClient} from "@angular/common/http";
import 'rxjs/add/operator/map';
import  'rxjs/add/operator/catch';
import 'rxjs/add/observable/throw';


@Injectable()
export class ConnectionService {
    websocketURL: string = window.location.href;
    private socket;
    defaultHeaders = {'Content-Type': 'application/json'};

    constructor(private http: HttpClient) {

    }

    public  setWsCallback(callback) {


        this.socket = io(this.websocketURL);
        this.socket.on('connect', (data) => {

            this.socket.on('data', (data) => {
                callback(data)
            });
        });

        return () => {
            this.socket.disconnect();
            this.socket = null;
        };
    }

    public post(q: string, data: any) {

        return this.http.post(this.websocketURL + q, data, {'headers': this.defaultHeaders})
            .map((result: any) => {
                this.socket.emit('url', data.camURL);
                //'rtsp://195.200.199.8/mpeg4/media.amp')
                // 'rtsp://184.72.239.149/vod/mp4:BigBuckBunny_175k.mov')
                return result;
            })

    }


}

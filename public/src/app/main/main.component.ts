import { Component, OnInit } from '@angular/core';
import {ConnectionService} from "../services/connection.service";

@Component({
  selector: 'app-main',
  templateUrl: './main.component.html',
  styleUrls: ['./main.component.css']
})
export class MainComponent implements OnInit {
  imgBase64:string
  hideSpinner:boolean=false
  model: any = {};
  constructor(private connection:ConnectionService) {

    connection.setWsCallback((data)=>{
      this.imgBase64=data;
      this.hideSpinner=true
    })
  }

  adding:boolean = true;
  ngOnInit() {



  }
  submit=()=>{

    this.connection.post('addCam', {camName:this.model.camName,camURL:this.model.camURL}).subscribe((data) => {
      this.adding = !this.adding;
      this.model.date=(data.date)?data.date:''
    });

  }

}

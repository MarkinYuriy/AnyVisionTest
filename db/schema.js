var mongoose = require('./mongoConnector');

var Schema = mongoose.Schema;

var camInfo = new Schema(
    {
        camName: {type: String, required: true},
        camURL: {
            type: String, required: true
        },
        date:{type:Date,required:true}

    },
    {
        strict: false, versionKey: false
    });

module.exports.camInfo = camInfo;
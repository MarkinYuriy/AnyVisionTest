
var mongoose = require('mongoose');

mongoose.Promise = require('bluebird');
mongoose.connect('mongodb://localhost:27017/cameras');

var con = mongoose.connection;

con.on('error', function(err){
    console.error.bind(console, 'connection error:')
});

con.on('connected', function (data) {
    console.log('Mongoose default connection open to ' + 'mongodb://localhost:27017/cameras');
});

// When the connection is disconnected
con.on('disconnected', function (err) {
    console.log('Mongoose default connection disconnected'['red']);
});

// If the Node process ends, close the Mongoose connection
process.on('SIGINT', function () {
    mongoose.connection.close(function () {
        console.log('Mongoose default connection disconnected through app termination');
        process.exit(0);
    });
});

con.once('open', function () {

});

mongoose.schOption = {
    versionKey: false,
    toJSON: {
        transform: function (doc, ret, callback) {
            ret.id = ret._id;
            delete ret._id;
        }
    },
    toObject: {
        transform: function (doc, ret, callback) {
            ret.id = ret._id;
            delete ret._id;
        }
    }
}

module.exports = mongoose;



var bodyParser = require('body-parser');
var express = require("express")
var app = express()
var rtsp = require('./lib/rtsp-ffmpeg');
/*var mongoose = require('./db/mongoConnector');*/
/*
var camSchema = require('./db/schema.js').camInfo;
*/
var path = require('path');

/*mongoose.model('camInfo', camSchema);*/

app.use((req, res, next) => {
    res.header("Access-Control-Allow-Origin", "*");
    res.header("Access-Control-Allow-Headers", "Origin,X-Requested-With,Content-Type,Accept,Cache-Control");
    res.header("Access-Control-Allow-Methods", "GET,PUT,POST,DELETE,OPTIONS");
    res.header("preflightContinue", true);
    next()
});


app.use(bodyParser.urlencoded({extended: true}));
app.use(bodyParser.json());

var http = require('http');
var server = http.createServer(
    app
).listen('6147', () => {
});

var io = require('socket.io')(server)


io.on('connection', (socket) => {
    let stream_ = {}

    let url = 'rtsp://195.200.199.8/mpeg4/media.amp';
    //'rtsp://184.72.239.149/vod/mp4:BigBuckBunny_175k.mov';
    socket.on('disconnect', () => {
        if (stream_.data && stream_.pipeStream) {
            stream_.data.removeListener('data', stream_.pipeStream);
        }
    });

   // socket.on('url', (data) => {

        stream = new rtsp.FFMpeg({input: url /*data*/});
        stream_.data = stream;
        stream.on('start', () => {
            console.log('stream ' + ' started');
        });


        var pipeStream = (data) => {
            src = 'data:image/jpeg;base64,' + data.toString('base64');
            socket.emit('data', src);
        };
        stream_.pipeStream = pipeStream;
        stream.on('data', pipeStream);
  //  });

});

//Obj = mongoose.model('camInfo');

app.use(express.static(path.join(__dirname, 'public/_')));
app.get('*', function (req, res) {
    res.redirect('/');
});

/*

app.post('/addCam', (req, res) => {
    req.body.date = new Date()
    var obj = new Obj(req.body);
    if (req.body) {
        Obj.create(obj, function (err, newObj) {
            if (err) {

                res.status(503).send({error: err.message});
            }
            else {
                res.status(200).send(newObj);
            }
        })
    } else {
        res.status(400).send("invalid request");
    }
});

*/
